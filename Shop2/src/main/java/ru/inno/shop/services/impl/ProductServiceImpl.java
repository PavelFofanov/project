package ru.inno.shop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.shop.dto.ProductForm;
import ru.inno.shop.models.Product;
import ru.inno.shop.repositories.ProductRepository;
import ru.inno.shop.services.ProductService;


import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllByStateNot(Product.State.DELETED);
    }

    @Override
    public void addProduct(ProductForm product) {
        Product newProduct = Product.builder()
                .state(Product.State.NOT_READY)
                .name(product.getName())
                .description("нет")
                .build();
        productRepository.save(newProduct);
    }

    @Override
    public void deleteProduct(Long productId) {
        Product product = productRepository.findById(productId).orElseThrow();
        product.setState(Product.State.DELETED);
        productRepository.save(product);
    }

    @Override
    public Product getProduct(Long productId) {
        return productRepository.findById(productId).orElseThrow();
    }

    @Override
    public void updateProduct(Long productId, ProductForm updateData) {
        Product productForUpdate = productRepository.findById(productId).orElseThrow();
        productForUpdate.setDescription(updateData.getDescription());

        productRepository.save(productForUpdate);
    }
}
