package ru.inno.shop.services;


import ru.inno.shop.dto.UserForm;
import ru.inno.shop.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long id, UserForm user);

    void deleteUser(Long id);
}
