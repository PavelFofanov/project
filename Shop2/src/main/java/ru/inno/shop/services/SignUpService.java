package ru.inno.shop.services;


import ru.inno.shop.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}
