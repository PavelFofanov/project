package ru.inno.shop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.shop.dto.CatalogForm;
import ru.inno.shop.models.Catalog;
import ru.inno.shop.models.Product;
import ru.inno.shop.models.User;
import ru.inno.shop.repositories.CatalogsRepository;
import ru.inno.shop.repositories.ProductRepository;
import ru.inno.shop.repositories.UsersRepository;
import ru.inno.shop.services.CatalogsService;


import java.util.List;

@Service
@RequiredArgsConstructor
public class CatalogsServiceImpl implements CatalogsService {

    private final CatalogsRepository catalogsRepository;

    private final UsersRepository usersRepository;

    private final ProductRepository productRepository;

    @Override
    public void addClientToCatalog(Long studentId, Long catalogId) {
        Catalog catalog = catalogsRepository.findById(catalogId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCatalog().add(catalog);

        usersRepository.save(student);
    }

    @Override
    public void addProductToCatalog(Long productId, Long catalogId) {
        Catalog catalog = catalogsRepository.findById(catalogId).orElseThrow();
        Product product = productRepository.findById(productId).orElseThrow();


        catalog.getProducts().add(product);
        product.setCatalog(catalog);

        productRepository.save(product);
        catalogsRepository.save(catalog);
    }

    @Override
    public Catalog getCatalog(Long id) {
        return catalogsRepository.findById(id).orElseThrow();
    }

    @Override
    public List<User> getNotInCatalogClients(Long catalogId) {
        Catalog catalog = catalogsRepository.findById(catalogId).orElseThrow();
        return usersRepository.findAllByCatalogNotContainsAndStateNot(catalog, User.State.DELETED);
    }

    @Override
    public List<Product> getNotInCatalogProducts() {
        return productRepository.findAllByCatalogNullAndStateNot(Product.State.DELETED);
    }

    @Override
    public List<Product> getInCatalogProducts(Long catalogId) {
        Catalog catalog = catalogsRepository.findById(catalogId).orElseThrow();
        return productRepository.findAllByCatalogAndStateNot(catalog, Product.State.DELETED);
    }

    @Override
    public List<User> getInCatalogClients(Long catalogId) {
        Catalog catalog = catalogsRepository.findById(catalogId).orElseThrow();
        return usersRepository.findAllByCatalogContainsAndStateNot(catalog, User.State.DELETED);
    }

    @Override
    public List<Catalog> getAllCatalogs() {
        return catalogsRepository.findAllByStateNot(Catalog.State.DELETED);
    }

    @Override
    public void addCatalog(CatalogForm catalog) {
        Catalog newCatalog = Catalog.builder()
                .title(catalog.getTitle())
                .description("описание каталога")
                .state(Catalog.State.NOT_READY)
                .build();

        catalogsRepository.save(newCatalog);
    }

    @Override
    public void deleteUser(Long id) {
        Catalog catalog = catalogsRepository.findById(id).orElseThrow();
        catalog.setState(Catalog.State.DELETED);
        catalogsRepository.save(catalog);
    }

    @Override
    public void updateCatalog(Long catalogId, CatalogForm catalog) {
        Catalog catalogForUpdate = catalogsRepository.findById(catalogId).orElseThrow();
        catalogForUpdate.setDescription(catalog.getDescription());
        catalogsRepository.save(catalogForUpdate);
    }
}
