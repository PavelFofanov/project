package ru.inno.shop.models;

import javax.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = "catalog")
@ToString(exclude = "catalog")
@Entity
public class Product {

    public enum State {
        READY, NOT_READY, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(length = 1000)

    private String description;

    @ManyToOne
    @JoinColumn(name = "catalog_id")
    private Catalog catalog;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
