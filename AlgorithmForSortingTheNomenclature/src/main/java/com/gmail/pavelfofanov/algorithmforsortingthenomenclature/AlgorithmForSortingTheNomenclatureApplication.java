package com.gmail.pavelfofanov.algorithmforsortingthenomenclature;

import com.gmail.pavelfofanov.algorithmforsortingthenomenclature.service.impl.NomenclatureServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgorithmForSortingTheNomenclatureApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgorithmForSortingTheNomenclatureApplication.class, args);

		NomenclatureServiceImpl nomenclatureService = new NomenclatureServiceImpl("input.txt");
		nomenclatureService.getAllSortedNomenclature().forEach(System.out::println);
	}

}
