package com.gmail.pavelfofanov.algorithmforsortingthenomenclature.service.impl;

import com.gmail.pavelfofanov.algorithmforsortingthenomenclature.model.Nomenclature;
import com.gmail.pavelfofanov.algorithmforsortingthenomenclature.service.NomenclatureService;
import lombok.AllArgsConstructor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class NomenclatureServiceImpl implements NomenclatureService {

    private final String fileName;

    @Override
    public List<String> getAllSortedNomenclature() {
        Stream<Nomenclature> concatSeAndDet = Stream.concat(getSortedNomenclatureSe().stream(),
                getSortedNomenclatureDet().stream());
        Stream<Nomenclature> concatStaAndMat = Stream.concat(getSortedNomenclatureStd().stream(),
                getSortedNomenclatureMat().stream());
        return Stream.concat(concatSeAndDet, concatStaAndMat)
                .map(nomenclatureToStringMapper)
                .collect(Collectors.toList());
    }

    private List<Nomenclature> getAllNomenclaturesFromFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            return bufferedReader
                    .lines()
                    .map(stringToNomenclatureMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Nomenclature> getSortedNomenclatureSe() {
        return getAllNomenclaturesFromFile()
                .stream()
                .filter(nomenclature -> nomenclature.getType().equals("SE"))
                .sorted(comparatorSeOrDet)
                .collect(Collectors.toList());
    }

    private List<Nomenclature> getSortedNomenclatureDet() {
        return getAllNomenclaturesFromFile()
                .stream()
                .filter(nomenclature -> nomenclature.getType().equals("DET"))
                .sorted(comparatorSeOrDet)
                .collect(Collectors.toList());
    }

    private List<Nomenclature> getSortedNomenclatureStd() {
        return getAllNomenclaturesFromFile()
                .stream()
                .filter(nomenclature -> nomenclature.getType().equals("STD"))
                .sorted(comparatorStd)
                .collect(Collectors.toList());
    }

    private List<Nomenclature> getSortedNomenclatureMat() {
        return getAllNomenclaturesFromFile()
                .stream()
                .filter(nomenclature -> nomenclature.getType().equals("MAT"))
                .sorted(Comparator.comparing(Nomenclature::getName))
                .collect(Collectors.toList());
    }

    private static final Function<String, Nomenclature> stringToNomenclatureMapper = currentNomenclature -> {
        String[] parts = currentNomenclature.split("\\   ");
        String type = parts[0];
        String name = parts[1];
        return new Nomenclature(type, name);
    };

    private static final Function<Nomenclature, String> nomenclatureToStringMapper = nomenclature ->
            nomenclature.getType() + "   " + nomenclature.getName();

    private static final Comparator<Nomenclature> comparatorSeOrDet = new Comparator<Nomenclature>() {
        @Override
        public int compare(Nomenclature o1, Nomenclature o2) {
            String[] partsO1 = o1.getName().split("\\-");
            Integer part1o1 = Integer.parseInt(partsO1[0]);
            Integer part2o1 = Integer.parseInt(partsO1[1]);
            Integer part3o1 = Integer.parseInt(partsO1[2]);
            String[] partsO2 = o2.getName().split("\\-");
            Integer part1o2 = Integer.parseInt(partsO2[0]);
            Integer part2o2 = Integer.parseInt(partsO2[1]);
            Integer part3o2 = Integer.parseInt(partsO2[2]);
            if (part2o1.equals(part2o2)) {
                if (part1o1.equals(part1o2)) {
                    return part3o1.compareTo(part3o2);
                }
            } else if (part2o1 > part2o2) {
                return 1;
            }
            return -1;
        }
    };

    private static final Comparator<Nomenclature> comparatorStd = new Comparator<Nomenclature>() {
        @Override
        public int compare(Nomenclature o1, Nomenclature o2) {
            String[] partsO1 = o1.getName().split("\\-");
            Integer part1o1 = Integer.parseInt(partsO1[1]);
            String[] partsO2 = o2.getName().split("\\-");
            Integer part1o2 = Integer.parseInt(partsO2[1]);
            if (part1o1.equals(part1o2) && partsO1.length == 4 && partsO2.length == 4) {
                if (partsO1[2].equals(partsO2[2])) {
                    return partsO1[3].compareTo(partsO2[3]);
                }
            } else if (part1o1.equals(part1o2) && partsO1.length == 3 && partsO2.length == 4) {
                return partsO1[2].compareTo(partsO2[3]);
            } else if (part1o1.equals(part1o2) && partsO1.length == 4 && partsO2.length == 3) {
                return partsO1[3].compareTo(partsO2[2]);
            } else if (part1o1 > part1o2) {
                return 1;
            }
            return -1;
        }
    };
}

