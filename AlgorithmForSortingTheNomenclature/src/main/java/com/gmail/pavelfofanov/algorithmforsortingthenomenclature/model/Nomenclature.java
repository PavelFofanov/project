package com.gmail.pavelfofanov.algorithmforsortingthenomenclature.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Nomenclature {
    private String type;
    private String name;
    }
