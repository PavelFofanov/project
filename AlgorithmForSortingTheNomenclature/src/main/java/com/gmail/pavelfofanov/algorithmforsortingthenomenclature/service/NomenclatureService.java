package com.gmail.pavelfofanov.algorithmforsortingthenomenclature.service;

import com.gmail.pavelfofanov.algorithmforsortingthenomenclature.model.Nomenclature;

import java.util.List;

public interface NomenclatureService {

    List<String> getAllSortedNomenclature();

}
