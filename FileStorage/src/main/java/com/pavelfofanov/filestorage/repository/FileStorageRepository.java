package com.pavelfofanov.filestorage.repository;

import com.pavelfofanov.filestorage.model.FileDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


@Repository
public interface FileStorageRepository extends JpaRepository<FileDB, Long> {

    List<FileDB> findAllByNameContains(String name);

    List<FileDB> findAllByDateOfChangeFileBetween(LocalDate from, LocalDate to);

    List<FileDB> findAllByTypeContainsOrderByName(String type);

//    List<FileDB> findAllByStateNot(FileDB.State state);
}
