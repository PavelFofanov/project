package com.pavelfofanov.filestorage.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.web.multipart.MultipartFile;


class MediaTypeValidator implements ConstraintValidator<ValidMediaType, MultipartFile> {

  @Override
  public void initialize(ValidMediaType constraintAnnotation) {
  }

  @Override
  public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext context) {

    boolean result = true;

    String contentType = multipartFile.getContentType();
    assert contentType != null;
    if (!isSupportedContentType(contentType)) {
      result = false;
    }
    return result;
  }

  private boolean isSupportedContentType(String contentType) {
    return contentType.equals("text/plain")
            || contentType.equals("application/pdf")
            || contentType.equals("application/zip")
            || contentType.equals("application/msword")
            || contentType.equals("application/vnd.oasis.opendocument.text")
            || contentType.equals("application/vnd.oasis.opendocument.spreadsheet")
            || contentType.equals("application/vnd.oasis.opendocument.presentation")
            || contentType.equals("application/vnd.oasis.opendocument.graphics")
            || contentType.equals("application/vnd.ms-excel")
            || contentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            || contentType.equals("application/vnd.ms-excel.sheet.macroEnabled.12")
            || contentType.equals("application/vnd.ms-powerpoint")
            || contentType.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")
            || contentType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            || contentType.equals("image/png")
            || contentType.equals("image/jpg")
            || contentType.equals("image/jpeg");
  }
}
