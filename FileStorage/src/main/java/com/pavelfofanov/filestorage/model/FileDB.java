package com.pavelfofanov.filestorage.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "file")
public class FileDB {

//    public enum State {
//        UPLOADED, DELETED
//    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long size;

    private String name;
    private String type;

    @Column(name = "date_of_upload")
    private LocalDate dateOfUpload;

    @Column(name = "date_of_change_file")
    private LocalDate dateOfChangeFile;

    @Lob
    private byte[] data;

    //    @Column(columnDefinition = "default UPLOADED")

//    @Enumerated(value = EnumType.STRING)
//    private State state;
}
