package com.pavelfofanov.filestorage.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Schema(description = "Informational Messages")
public class MessageDTO {

    @Schema(description = "Message", example = "Uploaded the file successfully:")
    private String message;
}
