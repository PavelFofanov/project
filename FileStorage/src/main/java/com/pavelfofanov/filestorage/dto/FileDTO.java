package com.pavelfofanov.filestorage.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@Builder
@Schema(description = "File")
public class FileDTO {

    @Schema(description = " File name", example = "test")
    private String name;
    @Schema(description = " File type", accessMode = Schema.AccessMode.READ_ONLY)
    private String type;

    @Schema(description = "File size", accessMode = Schema.AccessMode.READ_ONLY)
    private Long size;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDate dateOfUpload;
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDate dateOfChangeFile;

    @Schema(description = "File download URL", example = "http://localhost:8080/files/4857891953919418724")
    private String url;

}
