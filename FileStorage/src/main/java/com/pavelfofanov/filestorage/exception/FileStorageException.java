package com.pavelfofanov.filestorage.exception;

import com.pavelfofanov.filestorage.dto.MessageDTO;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class FileStorageException extends ResponseEntityExceptionHandler{

    @ExceptionHandler
    public ResponseEntity<MessageDTO> handleMaxSizeException(MaxUploadSizeExceededException e){
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body(new MessageDTO("File too large!"));
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<MessageDTO> noSuchElementException(NoSuchElementException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new MessageDTO("File not found"));
    }
    @ExceptionHandler
    public ResponseEntity<MessageDTO> handleValidationException(ValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new MessageDTO("Invalid Media Type"));
    }
}
