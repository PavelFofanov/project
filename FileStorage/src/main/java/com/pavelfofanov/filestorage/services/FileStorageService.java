package com.pavelfofanov.filestorage.services;

import com.pavelfofanov.filestorage.model.FileDB;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface FileStorageService {

    void store(MultipartFile file) throws IOException;

    FileDB getFileById(Long id);

    Stream<FileDB> getAllFiles();

    void deleteFile(Long id);

    List<FileDB> getAllFilesByNameContaining(String name);

    List<FileDB> getAllFilesByDateOfChangeFileBetween(LocalDate from, LocalDate to);

    List<FileDB> getAllFilesByTypeContains(String type);
}
