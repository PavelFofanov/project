package com.pavelfofanov.filestorage.services.impl;

import com.pavelfofanov.filestorage.model.FileDB;
import com.pavelfofanov.filestorage.repository.FileStorageRepository;
import com.pavelfofanov.filestorage.services.FileStorageService;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Primary
public class FileStorageServiceImpl implements FileStorageService {


    private final FileStorageRepository fileStorageRepository;

    @Override
    public void store(MultipartFile file) throws IOException {
        FileDB fileDB = FileDB.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .data(file.getBytes())
                .dateOfUpload(LocalDate.now())
                .dateOfChangeFile(LocalDate.now())
                .size(file.getSize())
                .build();
        fileStorageRepository.save(fileDB);
    }

    @Override
    public FileDB getFileById(Long id) {
        return fileStorageRepository.findById(id).orElseThrow();
    }

//    @Override
//    public Stream<FileDB> getAllFiles() {
//        return fileStorageRepository.findAllByStateNot(FileDB.State.DELETED).stream();
//    }

    @Override
    public Stream<FileDB> getAllFiles() {
        return fileStorageRepository.findAll().stream();
    }

    @Override
    public List<FileDB> getAllFilesByNameContaining(String name) {
            return fileStorageRepository.findAllByNameContains(name);
    }
    @Override
    public List<FileDB> getAllFilesByDateOfChangeFileBetween(LocalDate from, LocalDate to) {
            return fileStorageRepository.findAllByDateOfChangeFileBetween(from, to);
    }

    @Override
    public List<FileDB> getAllFilesByTypeContains(String type) {
        return fileStorageRepository.findAllByTypeContainsOrderByName(type);
    }

    @Override
    public void deleteFile(Long id) {
        fileStorageRepository.deleteById(id);
    }
}
