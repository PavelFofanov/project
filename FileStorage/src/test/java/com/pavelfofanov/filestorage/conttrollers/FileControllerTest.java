package com.pavelfofanov.filestorage.conttrollers;

import com.pavelfofanov.filestorage.model.FileDB;
import com.pavelfofanov.filestorage.services.impl.FileStorageServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class FileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileStorageServiceImpl fileStorageService;

    MockMultipartFile multipartFile = new MockMultipartFile("file", "test1.pdf",
            "application/json", "Spring Framework".getBytes());
    MockMultipartFile multipartFile1 = new MockMultipartFile("file", "test2.mp3",
            "application/json", "Spring Framework".getBytes());

    MockMultipartFile multipartBigFile = new MockMultipartFile("file", "testBigFile.mp4",
            "application/json", "Spring Framework".getBytes());

    Stream<FileDB> list = Stream.of(
            FileDB.builder()
                    .id(1L)
                    .size(multipartFile1.getSize())
                    .name(multipartFile1.getName())
                    .type(multipartFile1.getContentType())
                    .dateOfUpload(LocalDate.now())
                    .dateOfChangeFile(LocalDate.now())
                    .data(multipartFile1.getBytes()).build(),

            FileDB.builder()
                    .id(2L)
                    .size(multipartFile.getSize())
                    .name(multipartFile.getName())
                    .type(multipartFile.getContentType())
                    .dateOfUpload(LocalDate.now())
                    .dateOfChangeFile(LocalDate.now())
                    .data(multipartFile.getBytes()).build());

    FileControllerTest() throws IOException {
    }

    @Test
    void getListFiles() throws Exception {
        when(fileStorageService.getAllFiles()).thenReturn(list);

        mockMvc.perform(get("/files"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    void getFileById() throws Exception {

        when(fileStorageService.getFileById(anyLong())).thenReturn(FileDB.builder()
                .id(1L)
                .size(multipartFile1.getSize())
                .name(multipartFile1.getName())
                .type(multipartFile1.getContentType())
                .dateOfUpload(LocalDate.now())
                .dateOfChangeFile(LocalDate.now())
                .data(multipartFile1.getBytes()).build());
        mockMvc.perform(get("/files/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_OCTET_STREAM));
    }

    @Test
    void uploadFile() throws Exception {
        mockMvc.perform(multipart("/upload").file(multipartFile))
                .andExpect(status().isOk())
                .andReturn();
        verify(fileStorageService).store(multipartFile);
    }

    @Test
    void uploadFileBigFile() throws Exception {
        mockMvc.perform(multipart("/upload").file(multipartBigFile))
                .andExpect(status().is4xxClientError());
        verify(fileStorageService).store(multipartBigFile);


    }
}